import React from 'react';
import { ReactComponent as NombreDeMiIcono } from './iconos';
import { ReactComponent as CheckJPG } from './iconos/correcto.jpg';
import { ReactComponent as DeletePNG } from './iconos/cerrar.png';
import './TodoIcon.css';

function TodoIcon({ type, color, onClick }) {

    const iconTypes = {
        "Check": (
            <CheckJPG />
        ),
        "Delete": (
            <DeletePNG />
        ),
    };

    return (
        <span
            className={`Icon-container Icon-container--${type}`}
            onClick={onClick}
        >
            {/*aqui deberia ir el svg*/}</span>
    );

    <NombreDeMiIcono className="..." fill="color del icon"/>
}

export { TodoIcon };