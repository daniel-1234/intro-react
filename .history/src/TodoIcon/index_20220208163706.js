import React from 'react';
import { ReactComponent as NombreDeMiIcono } from './iconos';
import { ReactComponent as CheckJPG } from './iconos/correcto.jpg';
import { ReactComponent as DeletePNG } from './iconos/cerrar.png';
import './TodoIcon.css';


const iconTypes = {
    "Check": color => (
        <CheckJPG className="Icon-svg Icon-svg--check" fill={color}/>
    ),
    "Delete": (
        <DeletePNG className="Icon-svg Icon-svg--delete" fill={color}/>
    ),
};

function TodoIcon({ type, color = "gray", onClick }) {
    return (
        <span
            className={`Icon-container Icon-container--${type}`}
            onClick={onClick}
        >
            {iconTypes[type](color)}</span>
    );

    <NombreDeMiIcono className="..." fill="color del icon"/>
}

export { TodoIcon };