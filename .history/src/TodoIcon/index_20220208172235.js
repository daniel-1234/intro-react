import React from 'react';
import { ReactComponent as CheckJPG } from './correcto.jpg';
import { ReactComponent as DeletePNG } from './cerrar.png';
import './TodoIcon.css';


const iconTypes = {
    "Check": color => (
        <CheckJPG className="Icon-svg Icon-svg--check" fill={color}/>
    ),
    "Delete": (
        <DeletePNG className="Icon-svg Icon-svg--delete" fill={color}/>
    ),
};

function TodoIcon({ type, color = "gray", onClick }) {
    return (
        <span
            className={`Icon-container Icon-container--${type}`}
            onClick={onClick}
        >
            {iconTypes[type](color)}</span>
    );
    
}

export { TodoIcon };