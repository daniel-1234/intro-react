import React from "react";
import './TodosLoading.css';

function TodosLoading() {
    return <p>Estamos cargando, no desesperes...</p>;
}

export { TodosLoading };