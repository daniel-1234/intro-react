//import './App.css';

function App() {
  return (
    <todoCounter />
    <todoSearch />
    <todoList>
      <todoItem />
    </todoList>
  );
}

export default App;
