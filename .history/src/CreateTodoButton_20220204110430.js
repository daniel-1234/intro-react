import React from "react";

function CreateTodoButton(props) {
    return (
        <button className="CreateTodoButton">+</button>
    );
}

export { CreateTodoButton };