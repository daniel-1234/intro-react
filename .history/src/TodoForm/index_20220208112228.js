import React from 'react';

function TodoForm() {
    const onCancel = () => {
        //todo
    };

    const onSubmit = () => {
        //todo
    };

    return (
        <form onSubmit={onSubmit}>
            <label>...</label>
            <textarea 
                placeholder='Cortar la cebolla para el almuerzo'
            />
            <div>
                <button 
                    type='button'
                    onClick={onCancel}
                >
                    cancelar
                </button>

                <button
                    type='submit'
                >
                    Añadir
                </button>
            </div>
        </form>
    );
}

export { TodoForm };