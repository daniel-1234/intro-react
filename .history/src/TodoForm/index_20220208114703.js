import React from 'react';

function TodoForm() {
    const [ newTodoValue, setNewTodoValue ] = React.useState('');

    const {
        addTodo,
    } = React.useContext(TodoContext);


    const onChange = (event) => {
        setNewTodoValue(event.target.value);
    };

    const onCancel = () => {
        //todo
    };

    const onSubmit = (event) => {
        event.preventDefault();
        addTodo(newTodoValue);
    };

    return (
        <form onSubmit={onSubmit}>
            <label>...</label>
            <textarea 
                value={newTodoValue}
                onChange={onChange}
                placeholder='Cortar la cebolla para el almuerzo'
            />
            <div>
                <button 
                    type='button'
                    onClick={onCancel}
                >
                    cancelar
                </button>

                <button
                    type='submit'
                >
                    Añadir
                </button>
            </div>
        </form>
    );
}

export { TodoForm };